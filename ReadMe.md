Pour mon projet, j’ai créé directement mes données à l’aide de l’intelligence artificielle (voir PromptsChatGPT.pdf).

J’ai choisi comme sujet une entreprise de football 5v5 qui possèdent plusieurs centres en Suisse. Chaque centre possède un certain nombre de terrains qui sont réservés par des clients.

J’ai donc généré 4 fichiers .csv pour mes 4 tables (center, terrain, client et réservation) depuis chatGPT (copie des réponses dans vs code puis enregistrement en .csv). Dès cette étape, je gardais à l’esprit les différentes requêtes qui pourraient être intéressantes pour le projet de BI.
J’ai ensuite importé les fichiers sur Excel depuis mes fichiers csv. De base, je comptais importer mes données sur Storm depuis Excel. C’est ici que les problèmes ont commencé...

L’importation sur Excel ne fonctionnait pas du tout. J’ai bien fait toutes les étapes expliquées sur le séminaire, sur le wiki du cours, sur les pdf, mais rien n’a fonctionné. Je suis aussi allé regarder la docs sur le site stormcorp, mais cela n’a toujours pas été fructueux.
Après de nombreuses heures sans réussir à régler mon problème, j’ai eu une idée de bidouiller un peu en essayant de changer directement le fichier Excel de la démo en y insérant mes champs et données (comme la connexion est déjà censé fonctionner). C’est à ce moment la que je me suis rendu compte que même la démo Excel ne fonctionnait pas sur mon ordinateur (impossible d’inspecter les données depuis le schéma).
Je me suis donc dit que le problème venait de mon ordinateur (car il s’agit d’un ordi window que je n’ai pas utilisé depuis 3 ans). J’ai eu alors la bonne idée de réinitialiser mon ordinateur, en priant qu’une fois cela fait et storm téléchargé de nouveau, tout refonctionnerait normalement.
Cependant, cela ne fonctionnait toujours pas... J’ai donc pris la décision de passer par Mysql. Je suis allé voir les autres projets de demo qui utilisaient mysql et je m’en suis inspiré pour faire ma connexion. Après de nombreux essais, j’ai enfin réussi à mettre mes données sur Storm, avec le bon schéma (foreign, drilldown, ...)

Cependant, j’ai perdu énormément de temps lors des problèmes de configuration.

Je n’ai donc pas pu explorer comme je voulais les tableaux de bords et faire des pivots, graphique, ...
J’ai uniquement fait les choses de bases pour avoir quelque chose à montrer (notamment les drilldown).

J'ai passé environ une quinzaine d'heures sur ce projet (dont 10 surement pour régler des bugs).
