-- phpMyAdmin SQL Dump
-- version 5.1.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 22, 2023 at 11:34 AM
-- Server version: 5.7.24
-- PHP Version: 8.0.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sie_five`
--

-- --------------------------------------------------------

--
-- Table structure for table `centre`
--

CREATE TABLE `centre` (
  `id_centre` varchar(9) NOT NULL,
  `nom_centre` varchar(20) DEFAULT NULL,
  `ville` varchar(9) DEFAULT NULL,
  `capacite` varchar(8) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `centre`
--

INSERT INTO `centre` (`id_centre`, `nom_centre`, `ville`, `capacite`) VALUES
('CE1', 'Centre des Fraisiers', 'Genève', '8'),
('CE10', 'Centre des Vignes', 'Sion', '3'),
('CE2', 'Centre du Lion', 'Zurich', '8'),
('CE3', 'Centre de la Paix', 'Lausanne', '7'),
('CE4', 'Centre du Rhin', 'Bâle', '6'),
('CE5', 'Centre de l\'Ours', 'Berne', '5'),
('CE6', 'Centre des Alpes', 'Lucerne', '5'),
('CE7', 'Centre de la Sarine', 'Fribourg', '5'),
('CE8', 'Centre du Lac', 'Lugano', '4'),
('CE9', 'Centre de l\'Horloge', 'Neuchâtel', '4');

-- --------------------------------------------------------

--
-- Table structure for table `client`
--

CREATE TABLE `client` (
  `id_client` varchar(9) NOT NULL,
  `nom_client` varchar(17) DEFAULT NULL,
  `age` varchar(3) DEFAULT NULL,
  `sexe` varchar(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `client`
--

INSERT INTO `client` (`id_client`, `nom_client`, `age`, `sexe`) VALUES
('CL1', 'John Doe', '28', 'M'),
('CL10', 'Emma Jackson', '33', 'F'),
('CL100', 'Liam Taylor', '36', 'M'),
('CL11', 'Matthew Thompson', '38', 'M'),
('CL12', 'Ava White', '34', 'F'),
('CL13', 'Liam Harris', '30', 'M'),
('CL14', 'Sophia Clark', '35', 'F'),
('CL15', 'Noah Turner', '40', 'M'),
('CL16', 'Isabella Lewis', '26', 'F'),
('CL17', 'William Rodriguez', '37', 'M'),
('CL18', 'Charlotte Hill', '31', 'F'),
('CL19', 'Benjamin Green', '43', 'M'),
('CL2', 'Jane Smith', '32', 'F'),
('CL20', 'Mia Adams', '29', 'F'),
('CL21', 'Alexander Carter', '33', 'M'),
('CL22', 'Avery James', '38', 'F'),
('CL23', 'Ethan Phillips', '27', 'M'),
('CL24', 'Evelyn Martinez', '42', 'F'),
('CL25', 'Daniel Evans', '34', 'M'),
('CL26', 'Sofia Perez', '30', 'F'),
('CL27', 'Joseph Turner', '36', 'M'),
('CL28', 'Scarlett Cooper', '31', 'F'),
('CL29', 'Davidson Rivera', '39', 'M'),
('CL3', 'Michael Johnson', '45', 'M'),
('CL30', 'Chloe Rogers', '35', 'F'),
('CL31', 'Michael Hughes', '41', 'M'),
('CL32', 'Natalie Morgan', '26', 'F'),
('CL33', 'Matthew Adams', '43', 'M'),
('CL34', 'Ava Wilson', '29', 'F'),
('CL35', 'Andrew Martinez', '33', 'M'),
('CL36', 'Samantha Harris', '38', 'F'),
('CL37', 'Emma Carter', '30', 'F'),
('CL38', 'Oliver Johnson', '34', 'M'),
('CL39', 'Sophia Adams', '32', 'F'),
('CL4', 'Emily Davis', '39', 'F'),
('CL40', 'Emily Evans', '36', 'F'),
('CL41', 'Ethan Turner', '40', 'M'),
('CL42', 'Samantha Thompson', '26', 'F'),
('CL43', 'Oliver Clark', '34', 'M'),
('CL44', 'Charlotte Lewis', '31', 'F'),
('CL45', 'John Smith', '33', 'M'),
('CL46', 'Jane Johnson', '38', 'F'),
('CL47', 'Michael White', '27', 'M'),
('CL48', 'Emily Perez', '42', 'F'),
('CL49', 'David Harris', '39', 'M'),
('CL5', 'David Brown', '31', 'M'),
('CL50', 'Chloe Wilson', '35', 'F'),
('CL51', 'Matthew Carter', '41', 'M'),
('CL52', 'Natalie Adams', '29', 'F'),
('CL53', 'Liam Turner', '30', 'M'),
('CL54', 'Olivia Thompson', '36', 'F'),
('CL55', 'Jack Evans', '40', 'M'),
('CL56', 'Sophia Anderson', '28', 'F'),
('CL57', 'Oliver Taylor', '33', 'M'),
('CL58', 'Samantha Moore', '39', 'F'),
('CL59', 'Emma Davis', '35', 'F'),
('CL6', 'Sarah Wilson', '27', 'F'),
('CL60', 'Noah Martinez', '31', 'M'),
('CL61', 'Isabella Johnson', '37', 'F'),
('CL62', 'William Turner', '43', 'M'),
('CL63', 'Ava Clark', '29', 'F'),
('CL64', 'Matthew Hill', '38', 'M'),
('CL65', 'Olivia Martinez', '34', 'F'),
('CL66', 'David White', '31', 'M'),
('CL67', 'Ethan Adams', '36', 'M'),
('CL68', 'Michael Wilson', '42', 'M'),
('CL69', 'Chloe Johnson', '30', 'F'),
('CL7', 'Robert Taylor', '41', 'M'),
('CL70', 'Charlotte Perez', '33', 'F'),
('CL71', 'Samantha Clark', '39', 'F'),
('CL72', 'Emily Lewis', '35', 'F'),
('CL73', 'Oliver Thompson', '41', 'M'),
('CL74', 'Ava Adams', '29', 'F'),
('CL75', 'Emma Carter', '26', 'F'),
('CL76', 'John Harris', '37', 'M'),
('CL77', 'Michael Evans', '43', 'M'),
('CL78', 'Jane Turner', '32', 'F'),
('CL79', 'David Davis', '38', 'M'),
('CL8', 'Olivia Moore', '36', 'F'),
('CL80', 'Sophia Wilson', '34', 'F'),
('CL81', 'Matthew Hill', '40', 'M'),
('CL82', 'Olivia Adams', '27', 'F'),
('CL83', 'Liam Taylor', '42', 'M'),
('CL84', 'Alexander Clark', '39', 'M'),
('CL85', 'Charlotte Turner', '35', 'F'),
('CL86', 'Samantha Anderson', '41', 'F'),
('CL87', 'Oliver Moore', '31', 'M'),
('CL88', 'Ethan Johnson', '28', 'M'),
('CL89', 'Noah Perez', '33', 'M'),
('CL9', 'James Anderson', '29', 'M'),
('CL90', 'Emily Davis', '39', 'F'),
('CL91', 'Emma Thompson', '36', 'F'),
('CL92', 'James Clark', '45', 'M'),
('CL93', 'Ava Lewis', '29', 'F'),
('CL94', 'Isabella White', '32', 'F'),
('CL95', 'William Adams', '43', 'M'),
('CL96', 'Natalie Hill', '37', 'F'),
('CL97', 'Olivia Turner', '41', 'F'),
('CL98', 'Michael Carter', '45', 'M'),
('CL99', 'Emily Anderson', '32', 'F');

-- --------------------------------------------------------

--
-- Table structure for table `reservation`
--

CREATE TABLE `reservation` (
  `id_res` varchar(6) NOT NULL,
  `id_client` varchar(9) DEFAULT NULL,
  `id_terrain` varchar(10) DEFAULT NULL,
  `date_res` varchar(10) DEFAULT NULL,
  `heure_debut` varchar(11) DEFAULT NULL,
  `heure_fin` varchar(9) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `reservation`
--

INSERT INTO `reservation` (`id_res`, `id_client`, `id_terrain`, `date_res`, `heure_debut`, `heure_fin`) VALUES
('RES1', 'CL1', 'TE1', '01.01.2023', '11:00', '12:00'),
('RES10', 'CL10', 'TE16', '04.01.2023', '11:00', '12:00'),
('RES100', 'CL100', 'TE46', '04.02.2023', '11:00', '12:00'),
('RES101', 'CL23', 'TE48', '04.02.2023', '12:00', '13:00'),
('RES102', 'CL78', 'TE2', '04.02.2023', '13:00', '14:00'),
('RES103', 'CL55', 'TE4', '05.02.2023', '14:00', '15:00'),
('RES104', 'CL37', 'TE5', '05.02.2023', '15:00', '16:00'),
('RES105', 'CL67', 'TE7', '05.02.2023', '16:00', '17:00'),
('RES106', 'CL42', 'TE9', '06.02.2023', '17:00', '18:00'),
('RES107', 'CL19', 'TE11', '06.02.2023', '18:00', '19:00'),
('RES108', 'CL95', 'TE16', '06.02.2023', '19:00', '20:00'),
('RES109', 'CL32', 'TE17', '07.02.2023', '11:00', '12:00'),
('RES11', 'CL11', 'TE17', '04.01.2023', '12:00', '13:00'),
('RES110', 'CL81', 'TE19', '07.02.2023', '12:00', '13:00'),
('RES111', 'CL26', 'TE20', '07.02.2023', '13:00', '14:00'),
('RES112', 'CL8', 'TE21', '08.02.2023', '14:00', '15:00'),
('RES113', 'CL58', 'TE23', '08.02.2023', '15:00', '16:00'),
('RES114', 'CL49', 'TE25', '08.02.2023', '16:00', '17:00'),
('RES115', 'CL15', 'TE26', '09.02.2023', '17:00', '18:00'),
('RES116', 'CL11', 'TE28', '09.02.2023', '18:00', '19:00'),
('RES117', 'CL72', 'TE29', '09.02.2023', '19:00', '20:00'),
('RES118', 'CL64', 'TE31', '10.02.2023', '11:00', '12:00'),
('RES119', 'CL90', 'TE33', '10.02.2023', '12:00', '13:00'),
('RES12', 'CL12', 'TE18', '04.01.2023', '13:00', '14:00'),
('RES120', 'CL36', 'TE35', '10.02.2023', '13:00', '14:00'),
('RES121', 'CL50', 'TE37', '11.02.2023', '14:00', '15:00'),
('RES122', 'CL4', 'TE38', '11.02.2023', '15:00', '16:00'),
('RES123', 'CL77', 'TE40', '11.02.2023', '16:00', '17:00'),
('RES124', 'CL69', 'TE42', '12.02.2023', '17:00', '18:00'),
('RES125', 'CL16', 'TE43', '12.02.2023', '18:00', '19:00'),
('RES126', 'CL46', 'TE44', '12.02.2023', '19:00', '20:00'),
('RES127', 'CL83', 'TE45', '13.02.2023', '11:00', '12:00'),
('RES128', 'CL1', 'TE46', '13.02.2023', '12:00', '13:00'),
('RES129', 'CL100', 'TE48', '13.02.2023', '13:00', '14:00'),
('RES13', 'CL13', 'TE20', '05.01.2023', '14:00', '15:00'),
('RES130', 'CL59', 'TE1', '14.02.2023', '14:00', '15:00'),
('RES131', 'CL38', 'TE3', '14.02.2023', '15:00', '16:00'),
('RES132', 'CL65', 'TE5', '14.02.2023', '16:00', '17:00'),
('RES133', 'CL25', 'TE6', '15.02.2023', '17:00', '18:00'),
('RES134', 'CL84', 'TE8', '15.02.2023', '18:00', '19:00'),
('RES135', 'CL54', 'TE10', '15.02.2023', '19:00', '20:00'),
('RES136', 'CL13', 'TE12', '16.02.2023', '11:00', '12:00'),
('RES137', 'CL40', 'TE14', '16.02.2023', '12:00', '13:00'),
('RES138', 'CL92', 'TE15', '16.02.2023', '13:00', '14:00'),
('RES139', 'CL28', 'TE18', '17.02.2023', '14:00', '15:00'),
('RES14', 'CL14', 'TE21', '05.01.2023', '15:00', '16:00'),
('RES140', 'CL79', 'TE20', '17.02.2023', '15:00', '16:00'),
('RES141', 'CL20', 'TE22', '17.02.2023', '16:00', '17:00'),
('RES142', 'CL30', 'TE24', '18.02.2023', '17:00', '18:00'),
('RES143', 'CL74', 'TE26', '18.02.2023', '18:00', '19:00'),
('RES144', 'CL6', 'TE27', '18.02.2023', '19:00', '20:00'),
('RES145', 'CL88', 'TE30', '19.02.2023', '11:00', '12:00'),
('RES146', 'CL17', 'TE32', '19.02.2023', '12:00', '13:00'),
('RES147', 'CL96', 'TE34', '19.02.2023', '13:00', '14:00'),
('RES148', 'CL31', 'TE36', '20.02.2023', '14:00', '15:00'),
('RES149', 'CL85', 'TE38', '20.02.2023', '15:00', '16:00'),
('RES15', 'CL15', 'TE22', '05.01.2023', '16:00', '17:00'),
('RES150', 'CL41', 'TE40', '20.02.2023', '16:00', '17:00'),
('RES151', 'CL23', 'TE41', '21.02.2023', '17:00', '18:00'),
('RES152', 'CL55', 'TE42', '21.02.2023', '18:00', '19:00'),
('RES153', 'CL4', 'TE43', '21.02.2023', '19:00', '20:00'),
('RES154', 'CL67', 'TE45', '22.02.2023', '11:00', '12:00'),
('RES155', 'CL11', 'TE47', '22.02.2023', '12:00', '13:00'),
('RES156', 'CL50', 'TE48', '22.02.2023', '13:00', '14:00'),
('RES157', 'CL36', 'TE2', '23.02.2023', '14:00', '15:00'),
('RES158', 'CL14', 'TE3', '23.02.2023', '15:00', '16:00'),
('RES159', 'CL81', 'TE4', '23.02.2023', '16:00', '17:00'),
('RES16', 'CL16', 'TE24', '06.01.2023', '17:00', '18:00'),
('RES160', 'CL45', 'TE7', '24.02.2023', '17:00', '18:00'),
('RES161', 'CL2', 'TE9', '24.02.2023', '18:00', '19:00'),
('RES162', 'CL72', 'TE11', '24.02.2023', '19:00', '20:00'),
('RES163', 'CL29', 'TE13', '25.02.2023', '11:00', '12:00'),
('RES164', 'CL39', 'TE16', '25.02.2023', '12:00', '13:00'),
('RES165', 'CL71', 'TE17', '25.02.2023', '13:00', '14:00'),
('RES166', 'CL18', 'TE21', '26.02.2023', '14:00', '15:00'),
('RES167', 'CL56', 'TE23', '26.02.2023', '15:00', '16:00'),
('RES168', 'CL10', 'TE25', '26.02.2023', '16:00', '17:00'),
('RES169', 'CL61', 'TE29', '27.02.2023', '17:00', '18:00'),
('RES17', 'CL17', 'TE26', '06.01.2023', '18:00', '19:00'),
('RES170', 'CL78', 'TE31', '27.02.2023', '18:00', '19:00'),
('RES171', 'CL16', 'TE33', '27.02.2023', '19:00', '20:00'),
('RES172', 'CL95', 'TE35', '28.02.2023', '11:00', '12:00'),
('RES173', 'CL9', 'TE37', '28.02.2023', '12:00', '13:00'),
('RES174', 'CL86', 'TE39', '28.02.2023', '13:00', '14:00'),
('RES175', 'CL60', 'TE42', '01.03.2023', '14:00', '15:00'),
('RES176', 'CL17', 'TE43', '01.03.2023', '15:00', '16:00'),
('RES177', 'CL31', 'TE45', '01.03.2023', '16:00', '17:00'),
('RES178', 'CL92', 'TE47', '02.03.2023', '17:00', '18:00'),
('RES179', 'CL46', 'TE48', '02.03.2023', '18:00', '19:00'),
('RES18', 'CL18', 'TE27', '06.01.2023', '19:00', '20:00'),
('RES180', 'CL84', 'TE2', '02.03.2023', '19:00', '20:00'),
('RES181', 'CL27', 'TE3', '03.03.2023', '11:00', '12:00'),
('RES182', 'CL38', 'TE4', '03.03.2023', '12:00', '13:00'),
('RES183', 'CL73', 'TE7', '03.03.2023', '13:00', '14:00'),
('RES184', 'CL63', 'TE9', '04.03.2023', '14:00', '15:00'),
('RES185', 'CL48', 'TE11', '04.03.2023', '15:00', '16:00'),
('RES186', 'CL7', 'TE13', '04.03.2023', '16:00', '17:00'),
('RES187', 'CL68', 'TE16', '05.03.2023', '17:00', '18:00'),
('RES188', 'CL34', 'TE17', '05.03.2023', '18:00', '19:00'),
('RES189', 'CL6', 'TE21', '05.03.2023', '19:00', '20:00'),
('RES19', 'CL19', 'TE29', '07.01.2023', '11:00', '12:00'),
('RES190', 'CL79', 'TE23', '06.03.2023', '11:00', '12:00'),
('RES191', 'CL88', 'TE25', '06.03.2023', '12:00', '13:00'),
('RES192', 'CL15', 'TE29', '06.03.2023', '13:00', '14:00'),
('RES193', 'CL66', 'TE31', '07.03.2023', '14:00', '15:00'),
('RES194', 'CL19', 'TE33', '07.03.2023', '15:00', '16:00'),
('RES195', 'CL43', 'TE35', '07.03.2023', '16:00', '17:00'),
('RES196', 'CL22', 'TE37', '08.03.2023', '17:00', '18:00'),
('RES197', 'CL77', 'TE39', '08.03.2023', '18:00', '19:00'),
('RES198', 'CL33', 'TE42', '08.03.2023', '19:00', '20:00'),
('RES199', 'CL1', 'TE43', '09.03.2023', '11:00', '12:00'),
('RES2', 'CL2', 'TE2', '01.01.2023', '12:00', '13:00'),
('RES20', 'CL20', 'TE30', '07.01.2023', '12:00', '13:00'),
('RES200', 'CL50', 'TE45', '09.03.2023', '12:00', '13:00'),
('RES201', 'CL89', 'TE47', '09.03.2023', '13:00', '14:00'),
('RES202', 'CL54', 'TE48', '10.03.2023', '14:00', '15:00'),
('RES203', 'CL2', 'TE1', '10.03.2023', '15:00', '16:00'),
('RES204', 'CL13', 'TE3', '10.03.2023', '16:00', '17:00'),
('RES205', 'CL72', 'TE6', '11.03.2023', '17:00', '18:00'),
('RES206', 'CL24', 'TE9', '11.03.2023', '18:00', '19:00'),
('RES207', 'CL93', 'TE10', '11.03.2023', '19:00', '20:00'),
('RES208', 'CL37', 'TE12', '12.03.2023', '11:00', '12:00'),
('RES209', 'CL14', 'TE16', '12.03.2023', '12:00', '13:00'),
('RES21', 'CL21', 'TE32', '08.01.2023', '13:00', '14:00'),
('RES210', 'CL81', 'TE18', '12.03.2023', '13:00', '14:00'),
('RES211', 'CL35', 'TE20', '13.03.2023', '14:00', '15:00'),
('RES212', 'CL45', 'TE23', '13.03.2023', '15:00', '16:00'),
('RES213', 'CL52', 'TE26', '13.03.2023', '16:00', '17:00'),
('RES214', 'CL83', 'TE28', '14.03.2023', '17:00', '18:00'),
('RES215', 'CL49', 'TE30', '14.03.2023', '18:00', '19:00'),
('RES216', 'CL28', 'TE32', '14.03.2023', '19:00', '20:00'),
('RES217', 'CL96', 'TE34', '15.03.2023', '11:00', '12:00'),
('RES218', 'CL71', 'TE36', '15.03.2023', '12:00', '13:00'),
('RES219', 'CL31', 'TE38', '15.03.2023', '13:00', '14:00'),
('RES22', 'CL22', 'TE33', '08.01.2023', '14:00', '15:00'),
('RES220', 'CL18', 'TE41', '16.03.2023', '14:00', '15:00'),
('RES221', 'CL4', 'TE43', '16.03.2023', '15:00', '16:00'),
('RES222', 'CL98', 'TE45', '16.03.2023', '16:00', '17:00'),
('RES223', 'CL41', 'TE46', '17.03.2023', '17:00', '18:00'),
('RES224', 'CL67', 'TE48', '17.03.2023', '18:00', '19:00'),
('RES225', 'CL95', 'TE1', '17.03.2023', '19:00', '20:00'),
('RES226', 'CL11', 'TE3', '18.03.2023', '11:00', '12:00'),
('RES227', 'CL43', 'TE6', '18.03.2023', '12:00', '13:00'),
('RES228', 'CL27', 'TE9', '18.03.2023', '13:00', '14:00'),
('RES229', 'CL100', 'TE12', '19.03.2023', '14:00', '15:00'),
('RES23', 'CL23', 'TE35', '08.01.2023', '15:00', '16:00'),
('RES230', 'CL39', 'TE16', '19.03.2023', '15:00', '16:00'),
('RES231', 'CL88', 'TE18', '19.03.2023', '16:00', '17:00'),
('RES232', 'CL33', 'TE21', '20.03.2023', '17:00', '18:00'),
('RES233', 'CL59', 'TE23', '20.03.2023', '18:00', '19:00'),
('RES234', 'CL8', 'TE26', '20.03.2023', '19:00', '20:00'),
('RES235', 'CL19', 'TE30', '21.03.2023', '11:00', '12:00'),
('RES236', 'CL60', 'TE32', '21.03.2023', '12:00', '13:00'),
('RES237', 'CL90', 'TE36', '21.03.2023', '13:00', '14:00'),
('RES238', 'CL73', 'TE38', '22.03.2023', '14:00', '15:00'),
('RES239', 'CL84', 'TE41', '22.03.2023', '15:00', '16:00'),
('RES24', 'CL24', 'TE37', '09.01.2023', '16:00', '17:00'),
('RES240', 'CL79', 'TE44', '22.03.2023', '16:00', '17:00'),
('RES241', 'CL50', 'TE46', '23.03.2023', '17:00', '18:00'),
('RES242', 'CL22', 'TE48', '23.03.2023', '18:00', '19:00'),
('RES243', 'CL76', 'TE1', '23.03.2023', '19:00', '20:00'),
('RES244', 'CL58', 'TE5', '24.03.2023', '11:00', '12:00'),
('RES245', 'CL66', 'TE7', '24.03.2023', '12:00', '13:00'),
('RES246', 'CL48', 'TE11', '24.03.2023', '13:00', '14:00'),
('RES247', 'CL5', 'TE14', '25.03.2023', '14:00', '15:00'),
('RES248', 'CL37', 'TE17', '25.03.2023', '15:00', '16:00'),
('RES249', 'CL92', 'TE20', '25.03.2023', '16:00', '17:00'),
('RES25', 'CL25', 'TE38', '09.01.2023', '17:00', '18:00'),
('RES250', 'CL69', 'TE24', '26.03.2023', '17:00', '18:00'),
('RES251', 'CL31', 'TE29', '26.03.2023', '18:00', '19:00'),
('RES252', 'CL82', 'TE33', '26.03.2023', '19:00', '20:00'),
('RES253', 'CL20', 'TE35', '27.03.2023', '11:00', '12:00'),
('RES254', 'CL70', 'TE39', '27.03.2023', '12:00', '13:00'),
('RES255', 'CL15', 'TE42', '27.03.2023', '13:00', '14:00'),
('RES256', 'CL53', 'TE45', '28.03.2023', '14:00', '15:00'),
('RES257', 'CL14', 'TE2', '28.03.2023', '15:00', '16:00'),
('RES258', 'CL74', 'TE4', '28.03.2023', '16:00', '17:00'),
('RES259', 'CL36', 'TE10', '29.03.2023', '17:00', '18:00'),
('RES26', 'CL26', 'TE43', '10.01.2023', '18:00', '19:00'),
('RES260', 'CL91', 'TE13', '29.03.2023', '18:00', '19:00'),
('RES261', 'CL17', 'TE15', '29.03.2023', '19:00', '20:00'),
('RES262', 'CL55', 'TE19', '30.03.2023', '11:00', '12:00'),
('RES263', 'CL4', 'TE22', '30.03.2023', '12:00', '13:00'),
('RES264', 'CL68', 'TE25', '30.03.2023', '13:00', '14:00'),
('RES265', 'CL47', 'TE28', '31.03.2023', '14:00', '15:00'),
('RES266', 'CL52', 'TE31', '31.03.2023', '15:00', '16:00'),
('RES267', 'CL25', 'TE34', '31.03.2023', '16:00', '17:00'),
('RES268', 'CL6', 'TE37', '01.04.2023', '17:00', '18:00'),
('RES269', 'CL44', 'TE40', '01.04.2023', '18:00', '19:00'),
('RES27', 'CL27', 'TE45', '10.01.2023', '19:00', '20:00'),
('RES270', 'CL89', 'TE43', '01.04.2023', '19:00', '20:00'),
('RES271', 'CL34', 'TE6', '02.04.2023', '11:00', '12:00'),
('RES272', 'CL1', 'TE8', '02.04.2023', '12:00', '13:00'),
('RES273', 'CL42', 'TE12', '02.04.2023', '13:00', '14:00'),
('RES274', 'CL10', 'TE16', '03.04.2023', '14:00', '15:00'),
('RES275', 'CL38', 'TE18', '03.04.2023', '15:00', '16:00'),
('RES276', 'CL49', 'TE23', '03.04.2023', '16:00', '17:00'),
('RES277', 'CL96', 'TE27', '04.04.2023', '17:00', '18:00'),
('RES278', 'CL63', 'TE30', '04.04.2023', '18:00', '19:00'),
('RES279', 'CL7', 'TE36', '04.04.2023', '19:00', '20:00'),
('RES28', 'CL28', 'TE40', '11.01.2023', '11:00', '12:00'),
('RES280', 'CL57', 'TE1', '05.04.2023', '11:00', '12:00'),
('RES281', 'CL30', 'TE3', '05.04.2023', '12:00', '13:00'),
('RES282', 'CL84', 'TE7', '05.04.2023', '13:00', '14:00'),
('RES283', 'CL76', 'TE11', '06.04.2023', '14:00', '15:00'),
('RES284', 'CL18', 'TE14', '06.04.2023', '15:00', '16:00'),
('RES285', 'CL79', 'TE20', '06.04.2023', '16:00', '17:00'),
('RES286', 'CL26', 'TE24', '07.04.2023', '17:00', '18:00'),
('RES287', 'CL12', 'TE32', '07.04.2023', '18:00', '19:00'),
('RES288', 'CL73', 'TE35', '07.04.2023', '19:00', '20:00'),
('RES289', 'CL60', 'TE38', '08.04.2023', '11:00', '12:00'),
('RES29', 'CL29', 'TE1', '11.01.2023', '12:00', '13:00'),
('RES290', 'CL100', 'TE41', '08.04.2023', '12:00', '13:00'),
('RES291', 'CL21', 'TE44', '08.04.2023', '13:00', '14:00'),
('RES292', 'CL85', 'TE5', '09.04.2023', '14:00', '15:00'),
('RES293', 'CL40', 'TE9', '09.04.2023', '15:00', '16:00'),
('RES294', 'CL58', 'TE17', '09.04.2023', '16:00', '17:00'),
('RES295', 'CL23', 'TE21', '10.04.2023', '17:00', '18:00'),
('RES296', 'CL11', 'TE26', '10.04.2023', '18:00', '19:00'),
('RES297', 'CL66', 'TE33', '10.04.2023', '19:00', '20:00'),
('RES298', 'CL16', 'TE39', '11.04.2023', '11:00', '12:00'),
('RES299', 'CL87', 'TE42', '11.04.2023', '12:00', '13:00'),
('RES3', 'CL3', 'TE3', '01.01.2023', '13:00', '14:00'),
('RES30', 'CL30', 'TE2', '11.01.2023', '13:00', '14:00'),
('RES300', 'CL28', 'TE46', '11.04.2023', '13:00', '14:00'),
('RES301', 'CL34', 'TE48', '12.04.2023', '14:00', '15:00'),
('RES302', 'CL9', 'TE2', '12.04.2023', '15:00', '16:00'),
('RES303', 'CL98', 'TE6', '12.04.2023', '16:00', '17:00'),
('RES304', 'CL50', 'TE10', '13.04.2023', '17:00', '18:00'),
('RES305', 'CL44', 'TE13', '13.04.2023', '18:00', '19:00'),
('RES306', 'CL61', 'TE18', '13.04.2023', '19:00', '20:00'),
('RES307', 'CL25', 'TE22', '14.04.2023', '11:00', '12:00'),
('RES308', 'CL72', 'TE25', '14.04.2023', '12:00', '13:00'),
('RES309', 'CL38', 'TE29', '14.04.2023', '13:00', '14:00'),
('RES31', 'CL31', 'TE3', '12.01.2023', '14:00', '15:00'),
('RES310', 'CL10', 'TE34', '15.04.2023', '14:00', '15:00'),
('RES311', 'CL82', 'TE37', '15.04.2023', '15:00', '16:00'),
('RES312', 'CL1', 'TE40', '15.04.2023', '16:00', '17:00'),
('RES313', 'CL78', 'TE43', '16.04.2023', '17:00', '18:00'),
('RES314', 'CL32', 'TE47', '16.04.2023', '18:00', '19:00'),
('RES315', 'CL91', 'TE1', '16.04.2023', '19:00', '20:00'),
('RES316', 'CL19', 'TE4', '17.04.2023', '11:00', '12:00'),
('RES317', 'CL59', 'TE8', '17.04.2023', '12:00', '13:00'),
('RES318', 'CL41', 'TE12', '17.04.2023', '13:00', '14:00'),
('RES319', 'CL8', 'TE15', '18.04.2023', '14:00', '15:00'),
('RES32', 'CL32', 'TE5', '12.01.2023', '15:00', '16:00'),
('RES320', 'CL77', 'TE19', '18.04.2023', '15:00', '16:00'),
('RES321', 'CL47', 'TE23', '18.04.2023', '16:00', '17:00'),
('RES322', 'CL15', 'TE27', '19.04.2023', '17:00', '18:00'),
('RES323', 'CL70', 'TE31', '19.04.2023', '18:00', '19:00'),
('RES324', 'CL3', 'TE36', '19.04.2023', '19:00', '20:00'),
('RES325', 'CL67', 'TE2', '20.04.2023', '11:00', '12:00'),
('RES326', 'CL33', 'TE6', '20.04.2023', '12:00', '13:00'),
('RES327', 'CL92', 'TE11', '20.04.2023', '13:00', '14:00'),
('RES328', 'CL28', 'TE14', '21.04.2023', '14:00', '15:00'),
('RES329', 'CL56', 'TE20', '21.04.2023', '15:00', '16:00'),
('RES33', 'CL33', 'TE6', '12.01.2023', '16:00', '17:00'),
('RES330', 'CL17', 'TE24', '21.04.2023', '16:00', '17:00'),
('RES331', 'CL46', 'TE28', '22.04.2023', '17:00', '18:00'),
('RES332', 'CL63', 'TE33', '22.04.2023', '18:00', '19:00'),
('RES333', 'CL96', 'TE38', '22.04.2023', '19:00', '20:00'),
('RES334', 'CL51', 'TE41', '23.04.2023', '11:00', '12:00'),
('RES335', 'CL86', 'TE45', '23.04.2023', '12:00', '13:00'),
('RES336', 'CL39', 'TE3', '23.04.2023', '13:00', '14:00'),
('RES337', 'CL80', 'TE7', '24.04.2023', '14:00', '15:00'),
('RES338', 'CL7', 'TE11', '24.04.2023', '15:00', '16:00'),
('RES339', 'CL42', 'TE15', '24.04.2023', '16:00', '17:00'),
('RES34', 'CL34', 'TE9', '13.01.2023', '17:00', '18:00'),
('RES340', 'CL14', 'TE21', '25.04.2023', '17:00', '18:00'),
('RES341', 'CL97', 'TE24', '25.04.2023', '18:00', '19:00'),
('RES342', 'CL75', 'TE30', '25.04.2023', '19:00', '20:00'),
('RES343', 'CL52', 'TE33', '26.04.2023', '11:00', '12:00'),
('RES344', 'CL84', 'TE39', '26.04.2023', '12:00', '13:00'),
('RES345', 'CL64', 'TE43', '26.04.2023', '13:00', '14:00'),
('RES346', 'CL100', 'TE5', '27.04.2023', '14:00', '15:00'),
('RES347', 'CL88', 'TE9', '27.04.2023', '15:00', '16:00'),
('RES348', 'CL31', 'TE13', '27.04.2023', '16:00', '17:00'),
('RES349', 'CL2', 'TE18', '28.04.2023', '17:00', '18:00'),
('RES35', 'CL35', 'TE10', '13.01.2023', '18:00', '19:00'),
('RES350', 'CL21', 'TE22', '28.04.2023', '18:00', '19:00'),
('RES351', 'CL59', 'TE26', '28.04.2023', '19:00', '20:00'),
('RES352', 'CL48', 'TE32', '29.04.2023', '11:00', '12:00'),
('RES353', 'CL27', 'TE36', '29.04.2023', '12:00', '13:00'),
('RES354', 'CL70', 'TE40', '29.04.2023', '13:00', '14:00'),
('RES355', 'CL34', 'TE44', '30.04.2023', '14:00', '15:00'),
('RES356', 'CL9', 'TE6', '30.04.2023', '15:00', '16:00'),
('RES357', 'CL77', 'TE9', '30.04.2023', '16:00', '17:00'),
('RES358', 'CL6', 'TE14', '01.05.2023', '17:00', '18:00'),
('RES359', 'CL24', 'TE19', '01.05.2023', '18:00', '19:00'),
('RES36', 'CL36', 'TE11', '13.01.2023', '19:00', '20:00'),
('RES360', 'CL47', 'TE23', '01.05.2023', '19:00', '20:00'),
('RES361', 'CL30', 'TE27', '02.05.2023', '11:00', '12:00'),
('RES362', 'CL58', 'TE34', '02.05.2023', '12:00', '13:00'),
('RES363', 'CL38', 'TE39', '02.05.2023', '13:00', '14:00'),
('RES364', 'CL79', 'TE43', '03.05.2023', '14:00', '15:00'),
('RES365', 'CL13', 'TE4', '03.05.2023', '15:00', '16:00'),
('RES366', 'CL55', 'TE8', '03.05.2023', '16:00', '17:00'),
('RES367', 'CL66', 'TE13', '04.05.2023', '17:00', '18:00'),
('RES368', 'CL83', 'TE17', '04.05.2023', '18:00', '19:00'),
('RES369', 'CL35', 'TE22', '04.05.2023', '19:00', '20:00'),
('RES37', 'CL37', 'TE16', '14.01.2023', '11:00', '12:00'),
('RES370', 'CL89', 'TE25', '05.05.2023', '11:00', '12:00'),
('RES371', 'CL43', 'TE31', '05.05.2023', '12:00', '13:00'),
('RES372', 'CL18', 'TE35', '05.05.2023', '13:00', '14:00'),
('RES373', 'CL40', 'TE41', '06.05.2023', '14:00', '15:00'),
('RES374', 'CL22', 'TE45', '06.05.2023', '15:00', '16:00'),
('RES375', 'CL98', 'TE3', '06.05.2023', '16:00', '17:00'),
('RES376', 'CL50', 'TE12', '07.05.2023', '17:00', '18:00'),
('RES377', 'CL80', 'TE20', '07.05.2023', '18:00', '19:00'),
('RES378', 'CL2', 'TE28', '07.05.2023', '19:00', '20:00'),
('RES379', 'CL71', 'TE33', '08.05.2023', '11:00', '12:00'),
('RES38', 'CL38', 'TE17', '14.01.2023', '12:00', '13:00'),
('RES380', 'CL5', 'TE37', '08.05.2023', '12:00', '13:00'),
('RES381', 'CL42', 'TE45', '08.05.2023', '13:00', '14:00'),
('RES382', 'CL93', 'TE10', '09.05.2023', '14:00', '15:00'),
('RES383', 'CL32', 'TE15', '09.05.2023', '15:00', '16:00'),
('RES384', 'CL63', 'TE24', '09.05.2023', '16:00', '17:00'),
('RES385', 'CL78', 'TE29', '10.05.2023', '17:00', '18:00'),
('RES386', 'CL88', 'TE38', '10.05.2023', '18:00', '19:00'),
('RES387', 'CL26', 'TE44', '10.05.2023', '19:00', '20:00'),
('RES388', 'CL60', 'TE5', '11.05.2023', '11:00', '12:00'),
('RES389', 'CL99', 'TE11', '11.05.2023', '12:00', '13:00'),
('RES39', 'CL39', 'TE18', '14.01.2023', '13:00', '14:00'),
('RES390', 'CL11', 'TE16', '11.05.2023', '13:00', '14:00'),
('RES391', 'CL81', 'TE21', '12.05.2023', '14:00', '15:00'),
('RES392', 'CL37', 'TE30', '12.05.2023', '15:00', '16:00'),
('RES393', 'CL57', 'TE39', '12.05.2023', '16:00', '17:00'),
('RES394', 'CL74', 'TE1', '13.05.2023', '17:00', '18:00'),
('RES395', 'CL21', 'TE7', '13.05.2023', '18:00', '19:00'),
('RES396', 'CL64', 'TE14', '13.05.2023', '19:00', '20:00'),
('RES397', 'CL20', 'TE23', '14.05.2023', '11:00', '12:00'),
('RES398', 'CL76', 'TE32', '14.05.2023', '12:00', '13:00'),
('RES399', 'CL90', 'TE36', '14.05.2023', '13:00', '14:00'),
('RES4', 'CL4', 'TE4', '02.01.2023', '14:00', '15:00'),
('RES40', 'CL40', 'TE20', '15.01.2023', '14:00', '15:00'),
('RES400', 'CL49', 'TE42', '15.05.2023', '14:00', '15:00'),
('RES41', 'CL41', 'TE21', '15.01.2023', '15:00', '16:00'),
('RES42', 'CL42', 'TE22', '15.01.2023', '16:00', '17:00'),
('RES43', 'CL43', 'TE24', '16.01.2023', '17:00', '18:00'),
('RES44', 'CL44', 'TE26', '16.01.2023', '18:00', '19:00'),
('RES45', 'CL45', 'TE27', '16.01.2023', '19:00', '20:00'),
('RES46', 'CL46', 'TE29', '17.01.2023', '11:00', '12:00'),
('RES47', 'CL47', 'TE30', '17.01.2023', '12:00', '13:00'),
('RES48', 'CL48', 'TE32', '17.01.2023', '13:00', '14:00'),
('RES49', 'CL49', 'TE33', '18.01.2023', '14:00', '15:00'),
('RES5', 'CL5', 'TE5', '02.01.2023', '15:00', '16:00'),
('RES50', 'CL50', 'TE35', '18.01.2023', '15:00', '16:00'),
('RES51', 'CL51', 'TE36', '18.01.2023', '16:00', '17:00'),
('RES52', 'CL52', 'TE37', '19.01.2023', '17:00', '18:00'),
('RES53', 'CL53', 'TE38', '19.01.2023', '18:00', '19:00'),
('RES54', 'CL54', 'TE40', '19.01.2023', '19:00', '20:00'),
('RES55', 'CL55', 'TE43', '20.01.2023', '11:00', '12:00'),
('RES56', 'CL56', 'TE45', '20.01.2023', '12:00', '13:00'),
('RES57', 'CL57', 'TE46', '20.01.2023', '13:00', '14:00'),
('RES58', 'CL58', 'TE2', '21.01.2023', '14:00', '15:00'),
('RES59', 'CL59', 'TE4', '21.01.2023', '15:00', '16:00'),
('RES6', 'CL6', 'TE6', '02.01.2023', '16:00', '17:00'),
('RES60', 'CL60', 'TE5', '21.01.2023', '16:00', '17:00'),
('RES61', 'CL61', 'TE7', '22.01.2023', '17:00', '18:00'),
('RES62', 'CL62', 'TE9', '22.01.2023', '18:00', '19:00'),
('RES63', 'CL63', 'TE11', '22.01.2023', '19:00', '20:00'),
('RES64', 'CL64', 'TE16', '23.01.2023', '11:00', '12:00'),
('RES65', 'CL65', 'TE17', '23.01.2023', '12:00', '13:00'),
('RES66', 'CL66', 'TE19', '23.01.2023', '13:00', '14:00'),
('RES67', 'CL67', 'TE20', '24.01.2023', '14:00', '15:00'),
('RES68', 'CL68', 'TE21', '24.01.2023', '15:00', '16:00'),
('RES69', 'CL69', 'TE23', '24.01.2023', '16:00', '17:00'),
('RES7', 'CL7', 'TE9', '03.01.2023', '17:00', '18:00'),
('RES70', 'CL70', 'TE25', '25.01.2023', '17:00', '18:00'),
('RES71', 'CL71', 'TE26', '25.01.2023', '18:00', '19:00'),
('RES72', 'CL72', 'TE28', '25.01.2023', '19:00', '20:00'),
('RES73', 'CL73', 'TE29', '26.01.2023', '11:00', '12:00'),
('RES74', 'CL74', 'TE31', '26.01.2023', '12:00', '13:00'),
('RES75', 'CL75', 'TE32', '26.01.2023', '13:00', '14:00'),
('RES76', 'CL76', 'TE33', '26.01.2023', '14:00', '15:00'),
('RES77', 'CL77', 'TE35', '27.01.2023', '15:00', '16:00'),
('RES78', 'CL78', 'TE1', '27.01.2023', '16:00', '17:00'),
('RES79', 'CL79', 'TE3', '27.01.2023', '17:00', '18:00'),
('RES8', 'CL8', 'TE10', '03.01.2023', '18:00', '19:00'),
('RES80', 'CL80', 'TE6', '28.01.2023', '18:00', '19:00'),
('RES81', 'CL81', 'TE8', '28.01.2023', '19:00', '20:00'),
('RES82', 'CL82', 'TE10', '29.01.2023', '11:00', '12:00'),
('RES83', 'CL83', 'TE12', '29.01.2023', '12:00', '13:00'),
('RES84', 'CL84', 'TE13', '29.01.2023', '13:00', '14:00'),
('RES85', 'CL85', 'TE14', '30.01.2023', '14:00', '15:00'),
('RES86', 'CL86', 'TE15', '30.01.2023', '15:00', '16:00'),
('RES87', 'CL87', 'TE18', '30.01.2023', '16:00', '17:00'),
('RES88', 'CL88', 'TE22', '31.01.2023', '17:00', '18:00'),
('RES89', 'CL89', 'TE24', '31.01.2023', '18:00', '19:00'),
('RES9', 'CL9', 'TE11', '03.01.2023', '19:00', '20:00'),
('RES90', 'CL90', 'TE27', '31.01.2023', '19:00', '20:00'),
('RES91', 'CL91', 'TE30', '01.02.2023', '11:00', '12:00'),
('RES92', 'CL92', 'TE32', '01.02.2023', '12:00', '13:00'),
('RES93', 'CL93', 'TE34', '01.02.2023', '13:00', '14:00'),
('RES94', 'CL94', 'TE36', '02.02.2023', '14:00', '15:00'),
('RES95', 'CL95', 'TE39', '02.02.2023', '15:00', '16:00'),
('RES96', 'CL96', 'TE41', '02.02.2023', '16:00', '17:00'),
('RES97', 'CL97', 'TE43', '03.02.2023', '17:00', '18:00'),
('RES98', 'CL98', 'TE44', '03.02.2023', '18:00', '19:00'),
('RES99', 'CL99', 'TE45', '03.02.2023', '19:00', '20:00');

-- --------------------------------------------------------

--
-- Table structure for table `terrain`
--

CREATE TABLE `terrain` (
  `id_terrain` varchar(10) NOT NULL,
  `id_centre` varchar(9) DEFAULT NULL,
  `nom_centre` varchar(11) DEFAULT NULL,
  `statut` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `terrain`
--

INSERT INTO `terrain` (`id_terrain`, `id_centre`, `nom_centre`, `statut`) VALUES
('TE1', 'CE1', 'Geneve1', 'Ouvert'),
('TE10', 'CE3', 'Lausanne2', 'Ouvert'),
('TE11', 'CE3', 'Lausanne3', 'Ouvert'),
('TE12', 'CE3', 'Lausanne4', 'En travaux'),
('TE13', 'CE3', 'Lausanne5', 'Ouvert'),
('TE14', 'CE3', 'Lausanne6', 'Ouvert'),
('TE15', 'CE3', 'Lausanne7', 'Ouvert'),
('TE16', 'CE2', 'Zurich1', 'Ouvert'),
('TE17', 'CE2', 'Zurich2', 'Ouvert'),
('TE18', 'CE2', 'Zurich3', 'Ouvert'),
('TE19', 'CE2', 'Zurich4', 'En travaux'),
('TE2', 'CE1', 'Geneve2', 'Ouvert'),
('TE20', 'CE2', 'Zurich5', 'Ouvert'),
('TE21', 'CE2', 'Zurich6', 'Ouvert'),
('TE22', 'CE2', 'Zurich7', 'Ouvert'),
('TE23', 'CE2', 'Zurich8', 'Ouvert'),
('TE24', 'CE5', 'Berne1', 'Ouvert'),
('TE25', 'CE5', 'Berne2', 'En travaux'),
('TE26', 'CE5', 'Berne3', 'Ouvert'),
('TE27', 'CE5', 'Berne4', 'Ouvert'),
('TE28', 'CE5', 'Berne5', 'Ouvert'),
('TE29', 'CE8', 'Lugano1', 'Ouvert'),
('TE3', 'CE1', 'Geneve3', 'Ouvert'),
('TE30', 'CE8', 'Lugano2', 'Ouvert'),
('TE31', 'CE8', 'Lugano3', 'Fermé'),
('TE32', 'CE4', 'Bale1', 'Ouvert'),
('TE33', 'CE4', 'Bale2', 'Ouvert'),
('TE34', 'CE4', 'Bale3', 'Fermé'),
('TE35', 'CE4', 'Bale4', 'Ouvert'),
('TE36', 'CE4', 'Bale5', 'Ouvert'),
('TE37', 'CE6', 'Lucerne1', 'Ouvert'),
('TE38', 'CE6', 'Lucerne2', 'Ouvert'),
('TE39', 'CE6', 'Lucerne3', 'Fermé'),
('TE4', 'CE1', 'Geneve4', 'En travaux'),
('TE40', 'CE10', 'Sion1', 'Ouvert'),
('TE41', 'CE10', 'Sion2', 'Fermé'),
('TE42', 'CE9', 'Neuchatel1', 'Fermé'),
('TE43', 'CE9', 'Neuchatel2', 'Ouvert'),
('TE44', 'CE9', 'Neuchatel3', 'Ouvert'),
('TE45', 'CE7', 'Fribourg1', 'Ouvert'),
('TE46', 'CE7', 'Fribourg2', 'Ouvert'),
('TE47', 'CE7', 'Fribourg3', 'Fermé'),
('TE48', 'CE7', 'Fribourg4', 'Fermé'),
('TE5', 'CE1', 'Geneve5', 'Ouvert'),
('TE6', 'CE1', 'Geneve6', 'Ouvert'),
('TE7', 'CE1', 'Geneve7', 'Ouvert'),
('TE8', 'CE1', 'Geneve8', 'En travaux'),
('TE9', 'CE3', 'Lausanne1', 'Ouvert');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `centre`
--
ALTER TABLE `centre`
  ADD PRIMARY KEY (`id_centre`);

--
-- Indexes for table `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`id_client`);

--
-- Indexes for table `reservation`
--
ALTER TABLE `reservation`
  ADD PRIMARY KEY (`id_res`);

--
-- Indexes for table `terrain`
--
ALTER TABLE `terrain`
  ADD PRIMARY KEY (`id_terrain`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
